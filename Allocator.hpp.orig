// -----------
// Allocator.h
// -----------
#ifndef Allocator_h
#define Allocator_h
// --------
// includes
// --------
#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream> // cin, //cout
using namespace std;


// ---------
// Allocator
// ---------
template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------
    //keyword friend gives function access to private data
    friend bool operator == (const my_allocator& lhs, const my_allocator& rhs) {
        return (*lhs == *rhs);}                                                   // this is correct
    // -----------
    // operator !=
    // -----------
    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);}
    public:
        // --------
        // typedefs
        // --------
        using      value_type = T;
        using       size_type = std::size_t;
        using difference_type = std::ptrdiff_t;
        using       pointer   =       value_type*;
        using const_pointer   = const value_type*;
        using       reference =       value_type&;
        using const_reference = const value_type&;
    public:
        // ---------------
        // iterator
        // over the blocks
        // ---------------
        class iterator {
            // -----------
            // operator ==
            // -----------
            friend bool operator == (const iterator& lhs, const iterator& rhs) {
                return lhs._p == rhs._p;
                }                              
            // -----------
            // operator !=
            // -----------
            friend bool operator != (const iterator& lhs, const iterator& rhs) {
                return !(lhs == rhs);}
            private:
                // ----
                // data: pointer to the sentinel, compare the two addresses to check if
                //lhs 
                // ----
                int* _p;
            public:
                // -----------
                // constructor
                // -----------
                iterator (int* p) {
                    _p = p;}
                // ----------
                // operator *
                // ----------
                int& operator * () const {
                    //returns a reference to the sentinel 
                    //derefence the pointer/ value at the sentinel
                    return *_p;}  
                // -----------
                // operator ++
                // -----------
                //iterator needs to increase by the size of a type
                //can sizeof(value_type)
                //read in value of where _p and increment by size of block
                //correct way of using reinterpret_cast:
                //*reinterpret_cast<const int*>(&a[i])
                iterator& operator ++ () {
                    //get abs value at sentinnel (tells you how many bytes are being used)
                    int tmp = abs(*_p);
                    //jumping over beginning sentinnel value
                    //TODO: check if this is the correct way of casting
                    char* tmp_pointer = reinterpret_cast<char*>(_p) + 4;
                    //jumping over the block of bytes in use
                    tmp_pointer += tmp;
                    //add four again to get over the last sentinnel
                    tmp_pointer += 4;
                    //set p to tmp_pointer since we have skipped over block 
                    _p = reinterpret_cast<int*>(tmp_pointer);
                    //return the new spot
                    return *this;}
                // -----------
                // operator ++
                //returns the value at p
                // -----------
                iterator operator ++ (int) {
                    iterator x = *this;
                    ++*this;
                    return x;}
                // -----------
                // operator --
                // -----------
                iterator& operator -- () {
                    //get abs value at sentinnel (tells you how many bytes are being used)
                    //jumping over beginning sentinnel value
                    char* tmp_pointer = reinterpret_cast<char*>(_p) - 4;

                    int* value = reinterpret_cast<int*>(tmp_pointer);

                    int tmp = abs(*value);

                    //TODO: check if this is the correct way of casting

                    //jumping over the block of bytes in use
                    tmp_pointer -= tmp;

                    //add four again to get over the last sentinnel
                    tmp_pointer -= 4;

                    //set p to tmp_pointer since we have skipped over block 
                    _p = reinterpret_cast<int*>(tmp_pointer);
                    return *this;}
                // -----------
                // operator --
                // -----------
                iterator operator -- (int) {
                    iterator x = *this;
                    --*this;
                    return x;}};
        // ---------------
        // const_iterator
        // over the blocks
        // ---------------
        class const_iterator {
            // -----------
            // operator ==
            // -----------
            friend bool operator == (const const_iterator&, const const_iterator&) {
                // <your code>
                return false;}                                                       // replace!
            // -----------
            // operator !=
            // -----------
            friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
                return !(lhs == rhs);}
            private:
                // ----
                // data
                // ----
                
                //modified this -> ask if this is okay
                const int* _p;
            public:
                // -----------
                // constructor
                // -----------
                const_iterator (const int* p) {
                    _p = p;}
                // ----------
                // operator *
                //returns the value at p
                // ----------
                const int& operator * () const {
                    //returns a reference to the sentinel 
                    //derefence the pointer/ value at the sentinel
                    return *_p;}
                // -----------
                // operator ++
                // -----------
                const_iterator& operator ++ () {
                    //get abs value at sentinnel (tells you how many bytes are being used)
                    int tmp = abs(*_p);
                    //jumping over beginning sentinnel value
                    //TODO: check if this is the correct way of casting
                    const char* tmp_pointer = reinterpret_cast<const char*>(_p) + 4;
                    //jumping over the block of bytes in use
                    tmp_pointer += tmp;
                    //add four again to get over the last sentinnel
                    tmp_pointer += 4;
                    //set p to tmp_pointer since we have skipped over block 
                    _p = reinterpret_cast<const int*>(tmp_pointer);
                    //return the new spot
                    return *this;}
                // -----------
                // operator ++
                // -----------
                const_iterator operator ++ (int) {
                    const_iterator x = *this;
                    ++*this;
                    return x;}
                // -----------
                // operator --
                // -----------
                const_iterator& operator -- () {
                    //get abs value at sentinnel (tells you how many bytes are being used)
                    //jumping over beginning sentinnel value
                    const char* tmp_pointer = reinterpret_cast<const char*>(_p) - 4;

                    int* value = reinterpret_cast<const int*>(tmp_pointer);

                    int tmp = abs(*value);

                    //TODO: check if this is the correct way of casting

                    //jumping over the block of bytes in use
                    tmp_pointer -= tmp;

                    //add four again to get over the last sentinnel
                    tmp_pointer -= 4;

                    //set p to tmp_pointer since we have skipped over block 
                    _p = reinterpret_cast<const int*>(tmp_pointer);

                    return *this;}
                // -----------
                // operator --
                // -----------
                const_iterator operator -- (int) {
                    const_iterator x = *this;
                    --*this;
                    return x;}};
    private:
        // ----
        // data
        // ----
        //N isnt the number of T's, N is the number of bytes in allocator
        //add four extra bytes, TA told us to do this
        char a[N + 4];
        // -----
        // valid
        // -----
        /**
         * O(1) in space
         * O(n) in time
         * Need a valid set of blocks
         * March through entire array
         * confirm that the front and rear sentinel match in value and in sign 
         */
        bool valid () const {
            // // <your code>
            // // <use iterators>
            // const_iterator b = begin();
            // const_iterator e = end();

            // bool consec_positives = false;
            // int index = 0;

            // while(b != e) {
            //     // check to make sure no two consecutive block are positive
            //     if(consec_positives && *b > 0 ) {
            //         return false;
            //         //cout << "valid failed: 2 consec pos blocks" << endl;
            //     }
            //     // we are at a negative block
            //     else if(*b < 0 ) {
            //         consec_positives = false;
            //     }
            //     // starts a streak of 1 free block
            //     else {
            //         consec_positives = true;
            //     }
            //     // check the second sentinel
            //     index += abs(*b) + sizeof(int);
            //     if(*b != (*this)[index]) { 
            //         // if the two sentinals are not equal, return false
            //         //cout << "valid failed: sents !=" << endl;
            //         return false;
            //     }
            //     // update index to keep in line with pointer
            //     index += sizeof(int);
            //     b++; // increment pointer
            // }    
            return true;}
    public:
        // -----------
        // constructor
        // -----------
        /**
         * O(1) in space
         * O(1) in time
         * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
         */
        my_allocator () :a{0}{
            (*this)[0] = N - 8; // correct
            (*this)[N-4] = N - 8; // correct
            //(*this)[N + 4] = 0;
            assert(valid());}
        my_allocator             (const my_allocator&) = default;
        ~my_allocator            ()                    = default;
        my_allocator& operator = (const my_allocator&) = default;
        // --------
        // allocate
        // --------
        /**
         * O(1) in space
         * O(n) in time
         * after allocation there must be enough space left for a valid block
         * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
         * choose the first block that fits
         * throw a bad_alloc exception, if n is invalid
         */
        pointer allocate (size_type n) {
            //basic idea: we have an array of N bytes
            //iterate through array until we find the first free block that's big enough
            //for the size_type
            //once you find a free block 
            //first assign the beginning sentinnel, -40, skip 40 bytes, assign the end sentinnel 
            //assign the new free block sentinnels
            //use iterator to iterate through array a 
            iterator b = begin();
            iterator copy = begin();
            iterator e = end();
            //this is correct
            int num_alloc = n * sizeof(T);
            bool found_block = false;
            //iterate through array until you find first free block that's big enough for the
            //allocation request
            //while the block is busy and it's not big enough to use for the allocation request
            //move on to the next block
            int total_busy_bytes = 0;
            while(b != e && !found_block){
                if((*b > 0) && (*b >= num_alloc)){
                    total_busy_bytes += num_alloc + 8;
                    found_block = true;
                }else{
                    total_busy_bytes += (abs(*b) + 8);
                    b++;
                    copy++;
                }
            }

            int num_free = *b - num_alloc;
            ////cout << "num_free: " << num_free << endl;

            if(num_free == 0){
                ////cout << "in alloc if statement" << endl;
                int value = abs(*b);
                int* start = &(*b);
                int* end = start + 1 + (num_alloc / sizeof(int));
                *start = -(value); 
                *end = -(value);  
            }else if(num_free < sizeof(T) + (2 * sizeof(int))){
                ////cout << "in alloc else if statement" << endl;
                num_alloc = num_alloc + num_free;
                //set begining and end sentinal to new val 
                int* start = &(*b);
                int* end = start + 1 + (num_alloc / sizeof(int));
                *start = -(num_alloc);
                *end = -(num_alloc);
            }else{
                ////cout << "in alloc else statement" << endl;
                //allocate -> set the beginning sentinnel, and end sentinnel
                *b = -(num_alloc);

                int* start = &(*b);
                int* end = start + 1 + (num_alloc / sizeof(int));

                ////cout << "setting footer sentinel" << endl;
                *end = -(num_alloc);

                ////cout << "set footer sentinel: " << *end << endl;
                b++;
                ////cout << "b after jumping allocated block: " << *b << endl;

                start = &(*b);
                end = start + 1 + (num_free / sizeof(int));
                *start = num_free - 8;
                *end = num_free - 8;
            }

            assert(valid());
            return nullptr;}            // replace! -> throw a bad_alloc exception
        // ---------
        // construct
        // ---------
        /**
         * O(1) in space
         * O(1) in time
         */
        void construct (pointer p, const_reference v) {
            new (p) T(v);                               // this is correct and exempt
            assert(valid());}                           // from the prohibition of new
        // ----------
        // deallocate
        // ----------
        /**
         * O(1) in space
         * O(1) in time
         * after deallocation adjacent free blocks must be coalesced
         * throw an invalid_argument exception, if p is invalid
         * takes in a pointer of the index of the busy block
         */
        //TODO: do not use iterators, use pointers
    void deallocate (pointer p, size_type) {
            //if p is invalid
            if(p == nullptr){
                //throw invalid_argument exception
                throw invalid_argument("pointer p is invalid");
            } 
            //** WE MIGHT NEED TO DO THE SAME STEPS AS ABOVE IN ORDER TO GET THE POINTER PASSED IN INTO THE HEADER SPOT
            // get the size from the middle sentinal, and since we are freeing it, make sure to turn it into a positive number

            int* headerAddress = reinterpret_cast<int*>(p);

            //get to the header sentinel and read value
            --headerAddress;
            
            int middle_size = abs(*headerAddress);

            //pointer is at front sentinel
            iterator pointer = iterator(headerAddress);
            
            //--headerAddress;
            
            ////cout << "making left_pointer" <<endl;

            iterator left_pointer = iterator(headerAddress);
            left_pointer--;
            int left_value = *left_pointer;

            iterator right_pointer = iterator(headerAddress);
            right_pointer++;
            int right_value = *right_pointer;

            if(right_value > 0 && left_value > 0){
                int num_bytes = left_value + middle_size + right_value;
                *left_pointer = num_bytes + 16;
                int* start = &(*left_pointer);
                int* end = start + 1 + (num_bytes / sizeof(int));
                *end = num_bytes + 16;
            }else if(left_value > 0 && right_value < 0){
                int num_bytes = left_value + middle_size;
                *left_pointer = num_bytes + 8;
                ////cout << "setting value of left_pointer after freeing: " << *left_pointer <<endl;

                int* start = &(*left_pointer);
                int* end = start + 1 + (num_bytes / sizeof(int));

                ////cout << "setting footer sentinel" << endl;
                *end = num_bytes + 8;
                ////cout << "setting value of left_pointer footer after freeing: " << *left_pointer <<endl;

            }else if(left_value < 0 && right_value > 0 ){
                //cout << "coallescing right" <<endl;
                int num_bytes = right_value + middle_size;
                //cout << "num_bytes: " << num_bytes <<endl;
                *pointer = num_bytes + 8;
                //cout << "value at header of right_pointer after freeing: " << *pointer <<endl;
                int* start = &(*pointer);
                int* end = start + 1 + (num_bytes/ sizeof(int));
                *end = num_bytes + 8;
                //cout << "value at footer of right_pointer after freeing: " << *end <<endl;
            }else{
                ////cout << "went into else statement" << endl;
                *pointer = middle_size;
                ////cout << "value at pointer after freeing: " << *pointer <<endl;
                int* start = &(*pointer);
                int* end = start + 1 + (middle_size / sizeof(int));
                ////cout << "setting footer sentinel" << endl;
                *end = middle_size;
                ////cout << "value at pointer footer after freeing: " << *end <<endl;
            }
   
            assert(valid());}

 
        // -------
        // destroy
        // -------
        /**
         * O(1) in space
         * O(1) in time
         */
        void destroy (pointer p) {
            p->~T();               // this is correct
            assert(valid());}
        // -----------
        // operator []
        // -----------
        /**
         * O(1) in space
         * O(1) in time
         */
        int& operator [] (int i) {
            return *reinterpret_cast<int*>(&a[i]);}
        /**
         * O(1) in space
         * O(1) in time
         */
        const int& operator [] (int i) const {
            return *reinterpret_cast<const int*>(&a[i]);}
        // -----
        // begin
        // -----
        /**
         * O(1) in space
         * O(1) in time
         */
        iterator begin () {
            return iterator(&(*this)[0]);}
        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator begin () const {
            return const_iterator(&(*this)[0]);}
        // ---
        // end
        // ---
        /**
         * O(1) in space
         * O(1) in time
         */
        iterator end () {
            return iterator(&(*this)[N]);}
        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator end () const {
            return const_iterator(&(*this)[N]);}};
#endif // Allocator_h