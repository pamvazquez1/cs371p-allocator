import java.util.*;

public class Tester {

    public static void main(String[] args) {
        Random rand = new Random();

        for(int i = 0; i < 20; i++) {
            int num_lines =  rand.nextInt(29)+1;
            alloc_printer(num_lines, 8);
            System.out.println();
        }

    }

    public static void alloc_printer(int lines, int num_bytes) {
        int total_mem = 1000;
        Random random = new Random();
        boolean alloc = random.nextBoolean();
        Queue<Integer> mem_stored = new PriorityQueue<Integer>();
        for(int i = 0; i < lines; i ++) {
            //ths is the input, limiting this to 5 right now
            int rand_num = random.nextInt(4)+1;
            // 8 for the sent
            int bytes_alloced = 8 + (rand_num * num_bytes);
            // check if we must dealloc
            if(total_mem - bytes_alloced < 0) {
                alloc = false;
            }
            // check if we must alloc
            if(total_mem >= 1000 || mem_stored.peek() == null) {
                alloc = true;
            }
            // alloc if possible
            if(alloc) {
                // add to que
                total_mem -= bytes_alloced;
                mem_stored.add(bytes_alloced);
                System.out.println(rand_num);
            }
            // else dealloc
            else {
                total_mem += mem_stored.remove();
                System.out.println("-1");
            }
            assert(total_mem <= 1000);
        }
    }
}
