# CS371p: Object-Oriented Programming Allocator Repo

* Name: Pamela Vazquez De La Cruz, Jordan O'Dowd Hilder

* EID: pv5483, joh428

* GitLab ID: pamvazquez1

* HackerRank ID: joh428

* Git SHA: 615e32b6cecd44d92a01ecc5a0ff76ed6ee50a93

* GitLab Pipelines: https://gitlab.com/pamvazquez1/cs371p-allocator/-/pipelines

* Estimated completion time: Pamela: 18 hrs, Jordan: 20 hrs

* Actual completion time: Pamela: 50 hrs, Jordan: 50 hrs

* Comments: Hardest project yet, ate up all of my spring break  - Jordan
We also did over 75% of the project pair programming
