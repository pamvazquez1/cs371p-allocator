// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

//input format

//t: positive integer: number of test cases.
//Blank line.

//Blank line between two consecutive test cases.

//n lines: integers: either an allocation request or a deallocation request.

//Positive integers are the number of objects to allocate.

//Negative integers are the number of the busy block to free (one-based).

//object size is 8 bytes

//sample input 0

//4

//5 ---> allocate 5 objects 5*8 -> 40 bytes for just the objects, but we have to take the
//sentinels into account, sentinels are ints (size = 4 bytes) and you need two
// total bytes used = 48 bytes
//neg int -> in use
//heap: -40 bytes in use 952 free 948 944
//4 40 4 4 944 4
//-40 ......... -40 944 ...... 944 are free
//once you find a free block
//first assign the beginning sentinnel, -40, skip 40 bytes, assign the end sentinnel
//assign the new free block sentinnels

//keep adding block + end sentinnel to get to beginning of next free block (where the
//beginning sentinnel would be)
//the header sentinel is the ouput

//5
//3

//5
//3
//-1 -> deallocate request, index (one based) of the busy block to free

//5
//3
//3
//-1
//-1
//got rid of 5 and 3

#include <cassert>  // assert
#include <iostream> // cin, //cout
#include <string>   // getline, string, stoi
#include <sstream>  // istringstream
#include <vector>   // vector

using namespace std;

#include "Allocator.hpp"

const int MAX_CASES = 100;
const int MAX_LINES_PER_TESTCASE = 1000;
const int NUM_ALLOCATE_OBJ_SIZE = 8;
//template <typename T>;
//my_allocator<double, 1000> alloc
// TODO put assert 4 this

// read in until we have an empty line \n
void test_case_parser(istream& sin, ostream& sout) {
    string s;
    //define an allocator object for each test case
    //vector to hold the objects for each test case
    //define an iterator object so we can step through allocator object

    //* Had to change this condition

    //create allocator object
    my_allocator<double, 1000> alloc;
    //vector to hold in objects per test case
    vector<int> _objects_per_case;
    int num_objects;
    while(getline(sin, s) && s != "") {
        num_objects = stoi(s);
        //add num_objects to vector of objects, *might be able to use emplace_back to make it faster*
        _objects_per_case.push_back(num_objects);

    }

    //once you hit a new line then you want to allocate all the numbers in the vector
    for (int i = 0; i < _objects_per_case.size(); ++i) {
        //call allocate on that allocator_object.allocate(num_objects);
        double object_to_allocate = _objects_per_case[i];

        if(object_to_allocate > 0) {
            //allocate
            //sout<< "going into allocate method" << endl;
            alloc.allocate(object_to_allocate);
            //sout << "done allocating " << endl;
        } else {
            //deallocate, takes in a pointer so what we have written below is incorrect!
            my_allocator<double, 1000>::iterator b = alloc.begin();
            my_allocator<double, 1000>::iterator e = alloc.end();

            //sout << "address of beginning " << &b << endl;
            //sout<< "in else statement for deallocate" << endl;
            int index = 1;
            double* target = nullptr;
            bool found = false;

            while(!found && b != e) {
                //sout<< "index of busy block: " << object_to_allocate << endl;
                //sout<< "in while loop to find pointer for deallocate" << endl;

                if(*b < 0 && index == abs(object_to_allocate)) {
                    //sout<< "in while loop if statement, target has been found" << endl;
                    //b is at target pointer -> cast iterator to pointer and return
                    //target = reinterpret_cast<double*>(&(*b));
                    //int* tmp =
                    target = reinterpret_cast<double*>(reinterpret_cast<int*>(&(*b) + 1));
                    //sout << "b pointer value *b: " << *b << endl;
                    //sout << "b pointer address &b: " << &b <<endl;
                    //sout << "address of target pointer: reinterpret_cast<double*>(*b): " << &target << endl;
                    found = true;
                } else if(*b < 0 && index != abs(object_to_allocate)) {
                    index++;
                    b++;
                } else {
                    b++;
                }
            }

            //sout << "outside while loop, should have found pointer, index: " << index << endl;
            //sout << "outside while loop, target pointer: " << target << endl;
            alloc.deallocate(target, object_to_allocate);
        }

        //debugging purposes
        //std:://cout << *i << ' ';
    }

    my_allocator<double, 1000>::iterator b = begin(alloc);
    my_allocator<double, 1000>::iterator e = end(alloc);
    //sout << " debug b " << *b << " " << endl;

    if(b != e) {
        //sout << "in if statement to print" << endl;;
        sout << *b;
        b++;
    }
    while(b != e) {
        //sout << "in while loop to print" << endl;
        sout << " " << *b;
        b++;
    }

    sout << endl;


}

// This method parses the input and aims to solve the voting problem
//last test case ends end of file!!!
void allocate_solve(istream& sin, ostream& sout) {
    // first line is the number of test cases
    string s;
    getline(sin, s);
    int num_testcases;

    //sout << s << endl;
    num_testcases = stoi(s);

    //DEBUG to see if we are reading in correctly
    //sout << num_testcases << endl;
    getline(sin, s); //skip empty line

    // 0 < testcases <= 100
    assert(0 < num_testcases);
    assert(num_testcases <= MAX_CASES);

    // iterate through all the test cases
    int num = 1;
    while(num_testcases > 0) {
        //read in test case
        test_case_parser(sin, sout);
        // sout << "solved test case number: " << num <<endl;
        num++;
        num_testcases--;
    }
}



// ----
// main
// ----

int main () {
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */

    allocate_solve(cin, cout);
    return 0;
}

