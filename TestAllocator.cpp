// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s); //allocating 10 ints due to allocator_type
    if (b != nullptr) { //if allocate call fails, it will return null pointer, confirm the real allocator does this
        pointer e = b + s;  //pointer to end
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v); //construct each object
                ++p;
            }
        } //move to the next element
        catch (...) {   //catch any exception that's thrown
            while (b != p) {    //iterate until variable p
                --p;            //back out
                x.destroy(p);
            }  //destroy each object
            x.deallocate(b, s); //deallocate the whole space
            throw;
        } //rethrow exception to caller
        //if nothing went wrong w construction
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));

        //throw it all away
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

/* only for my_allocator                                                         // uncomment
TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 0);}                                         // fix test

TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 0);}                                         // fix test
*/

// ----
// allocate tests
// ----


TEST(AllocatorFixture, allocate_test0) {

}

// ----
// test_case_parser tests
// ----

// Test to make sure that equal allocs and deallocs create a free block
TEST(AllocatorFixture, test_case_parser_test0) {
    string correct_output = "992\n";
    std::stringstream sin;
    sin << "1\n" << endl;
    sin << "1\n" << endl;
    sin << "-1\n" << endl;
    sin << "-1\n" << endl;
    ASSERT_EQ(test_case_parser(sin), correct_output);
}

// Test to make sure that indexes less than -1 deallocate correctly
TEST(AllocatorFixture, test_case_parser_test1) {
    string correct_output = "992\n";
    std::stringstream sin;
    sin << "5\n" << endl;
    sin << "3\n" << endl;
    sin << "-2\n" << endl;
    sin << "-1\n" << endl;
    ASSERT_EQ(test_case_parser(sin), correct_output);
}

// Test to make sure that biggest block is filled up correctly
TEST(VotingFixture, test_case_parser_test2) {
    string correct_output = "-992\n";
    std::stringstream sin;
    sin << "124\n" << endl;
    ASSERT_EQ(test_case_parser(sin), correct_output);
}

// Test to make sure that second biggest block is filled up correctly, and the left over 8 bytes are given to the block
TEST(AllocatorFixture, test_case_parser_test3) {
    string correct_output = "-992\n";
    std::stringstream sin;
    sin << "123\n" << endl;
    ASSERT_EQ(test_case_parser(sin), correct_output);
}

// Test to make sure that biggest block with leftovers is filled up correctly
TEST(AllocatorFixture, test_case_parser_test4) {
    string correct_output = "-976 8\n";
    std::stringstream sin;
    sin << "122\n" << endl;
    ASSERT_EQ(test_case_parser(sin), correct_output);
}

// Test to make sure that right coalese works propperly
TEST(AllocatorFixture, test_case_parser_test5) {
    string correct_output = "-40 944\n";
    std::stringstream sin;
    sin << "5\n" << endl;
    sin << "5\n" << endl;
    sin << "5\n" << endl;
    sin << "-3\n" << endl;
    sin << "-2\n" << endl;
    ASSERT_EQ(test_case_parser(sin), correct_output);
}

// Test to make sure that it can coalese from both sides
TEST(AllocatorFixture, test_case_parser_test6) {
    string correct_output = "992\n";
    std::stringstream sin;
    sin << "5\n" << endl;
    sin << "5\n" << endl;
    sin << "5\n" << endl;
    sin << "-3\n" << endl;
    sin << "-1\n" << endl;
    sin << "-1\n" << endl;
    ASSERT_EQ(test_case_parser(sin), correct_output);
}

// ----
// allocate_solve tests
// ----

// Test to make sure that allocate solve can solve the same testcase as test case parser
TEST(AllocatorFixture, allocate_solve0) {
    std::stringstream sin("1\n\n1\n1\n-1\n-1");
    ostringstream sout;
    allocate_solve(sin, sout);
    ASSERT_EQ(sout.str(), "992\n");
}

// Test to make sure that allocate solve can solve multiple test cases
TEST(AllocatorFixture, allocate_solve1) {
    std::stringstream sin("2\n\n1\n1\n-1\n-1\n\n124\n");
    ostringstream sout;
    allocate_solve(sin, sout);
    ASSERT_EQ(sout.str(), "992\n-992\n");
}

// Test to make sure that allocate solve can solve multiple test that are the same
TEST(AllocatorFixture, allocate_solve2) {
    std::stringstream sin("2\n\n1\n1\n-1\n-1\n\n1\n1\n-1\n-1\n");
    ostringstream sout;
    allocate_solve(sin, sout);
    ASSERT_EQ(sout.str(), "992\n922\n");
}

// ----
// equal_equals_opp tests
// ----

// Test to make sure that my allocator can correctly test if two values that are equal, return equal
TEST(AllocatorFixture, equal_equals_opp0) {
    my_allocator<double, 1000>::iterator b = alloc.end();
    my_allocator<double, 1000>::iterator e = alloc.end();
    bool check = (b == e);
    ASSERT_EQ(check, true);
}

// Test to make sure that my allocator can correctly test if two values that are not equal, return false
TEST(AllocatorFixture, equal_equals_opp1) {
    my_allocator<double, 1000>::iterator b = alloc.begin();
    my_allocator<double, 1000>::iterator e = alloc.end();
    bool check = (b == e);
    ASSERT_EQ(check, false);
}

// ----
// not_equals_opp tests
// ----

// Test to make sure that my allocator can correctly test if two values that are equal, return equal
TEST(AllocatorFixture, not_equals_opp0) {
    my_allocator<double, 1000>::iterator b = alloc.begin();
    my_allocator<double, 1000>::iterator e = alloc.end();
    bool check = (b != e);
    ASSERT_EQ(check, true);
}

// Test to make sure that my allocator can correctly test if two values that are equal, return equal
TEST(AllocatorFixture, not_equals_opp1) {
    my_allocator<double, 1000>::iterator b = alloc.end();
    my_allocator<double, 1000>::iterator e = alloc.end();
    bool check = (b != e);
    ASSERT_EQ(check, false);
}

// -----------
// operator ++ tests
// -----------

TEST(AllocatorFixture, not_equals_opp0) {
    my_allocator<double, 1000>::iterator b = alloc.begin();
    my_allocator<double, 1000>::iterator e = alloc.end();
    while(b != e) {
        b++;
    }
    bool check = (b == e);
    ASSERT_EQ(check, true);
}

// -----------
// operator -- tests
// -----------

TEST(AllocatorFixture, not_equals_opp0) {
    my_allocator<double, 1000>::iterator b = alloc.begin();
    my_allocator<double, 1000>::iterator e = alloc.end();
    while(b != e) {
        e--;
    }
    bool check = (b == e);
    ASSERT_EQ(check, true);
}

// ----------
// operator * tests
// ----------

TEST(AllocatorFixture, star_opp0) {
    my_allocator<double, 1000>::iterator b = alloc.begin();
    *b = 322;
    ASSERT_EQ(*b, 322);
}