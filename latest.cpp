// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream> // cin, cout

using namespace std;

//list<int> x = {2, 3, 4}
//list<int> :: iterator b = being(x);
//list<int> :: iterator e = begin(x);

//while( b != e)
    //*b represents the elements of the list

//my_allocator<int, 100> x = {2, 3, 4}
//.. calls to allocate, deallocate
//my_allcoator<> :: iterator b = being(x);
//my_allcoator<> :: iterator e = begin(x);

//while( b != e)
    //*b represents the front sentinel of the blocks of the allocator


// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------
    //keyword friend gives function access to private data
    friend bool operator == (const my_allocator& lhs, const my_allocator& rhs) {
        return false;}                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);}

    public:
        // --------
        // typedefs
        // --------

        using      value_type = T;

        using       size_type = std::size_t;
        using difference_type = std::ptrdiff_t;

        using       pointer   =       value_type*;
        using const_pointer   = const value_type*;

        using       reference =       value_type&;
        using const_reference = const value_type&;

    public:
        // ---------------
        // iterator
        // over the blocks
        // ---------------

        class iterator {
            // -----------
            // operator ==
            // -----------

            friend bool operator == (const iterator& lhs, const iterator& rhs) {
                //return lhs._p == rhs._p;
                return (*lhs == *rhs);
                }                              

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const iterator& lhs, const iterator& rhs) {
                return !(lhs == rhs);}

            private:
                // ----
                // data: pointer to the sentinel, compare the two addresses to check if
                //lhs 
                // ----

                int* _p;

            public:
                // -----------
                // constructor
                // -----------

                iterator (int* p) {
                    _p = p;}

                // ----------
                // operator *
                // ----------

                int& operator * () const {
                    //returns a reference to the sentinel 

                    //char* copy = reinterpret_cast<char*>(_p);

                    //copy += (4 + (abs(*_p));
                    
                    //set value at the end sentinnel equal to value at _p (which is the beginning sentinnel)
                    //*copy = *_p;

                    //derefence the pointer/ value at the sentinel
                    return *_p;}  

                // -----------
                // operator ++
                // -----------
                //iterator needs to increase by the size of a type
                //can sizeof(value_type)
                //read in value of where _p and increment by size of block
                //correct way of using reinterpret_cast:
                //*reinterpret_cast<const int*>(&a[i])
                iterator& operator ++ () {
                    //cout << "before incrementing p: " << endl;
                    //get abs value at sentinnel (tells you how many bytes are being used)
                    int tmp = abs(*_p);

                    //cout << "value of temp: " << tmp << endl;
                    //jumping over beginning sentinnel value
                    //TODO: check if this is the correct way of casting

                    char* tmp_pointer = reinterpret_cast<char*>(_p) + 4;

                    //cout << "address of tmp_pointer after casting to char +4 : " << &tmp_pointer << endl;
                    //jumping over the block of bytes in use
                    tmp_pointer = tmp_pointer + tmp;

                    //cout << "address of tmp_pointer after adding tmp: " << &tmp_pointer << endl;

                    //add four again to get over the last sentinnel
                    tmp_pointer = tmp_pointer + 4;

                    //cout << "address of tmp_pointer after adding tmp + 4: " << &tmp_pointer << endl;

                    //int* int_temp = reinterpret_cast<int*>(tmp_pointer); 

                    //cout << "address of tmp_pointer after casting back to int pointer: " << &tmp_pointer << endl;
                    //set p to tmp_pointer since we have skipped over block 
                    _p = reinterpret_cast<int*>(tmp_pointer);


                    //cout << "after incrementing p: " << endl;
                    //int value = *_p < 0 ? 0 - *_p : *_p;
                    //_p = _p + 1 + (value /sizeof(int)) + 1;
                    //return the new spot
                    return *this;}

                // -----------
                // operator ++
                //returns the value at p
                // -----------

                iterator operator ++ (int) {
                    iterator x = *this;
                    ++*this;
                    return x;}

                // -----------
                // operator --
                // -----------

                iterator& operator -- () {
                    //get abs value at sentinnel (tells you how many bytes are being used)
                    //jumping over beginning sentinnel value
                    char* tmp_pointer = reinterpret_cast<char*>(_p) - 4;

                    int* value = reinterpret_cast<int*>(tmp_pointer);

                    int tmp = abs(*value);

                    //TODO: check if this is the correct way of casting

                    //jumping over the block of bytes in use
                    tmp_pointer -= tmp;

                    //add four again to get over the last sentinnel
                    tmp_pointer -= 4;

                    //set p to tmp_pointer since we have skipped over block 
                    _p = reinterpret_cast<int*>(tmp_pointer);

                    //return the new spot
                    return *this;}

                // -----------
                // operator --
                // -----------

                iterator operator -- (int) {
                    iterator x = *this;
                    --*this;
                    return x;}};

        // ---------------
        // const_iterator
        // over the blocks
        // ---------------

        class const_iterator {
            // -----------
            // operator ==
            // -----------

            friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
                // <your code>
                return lhs._p == rhs._p;}                                                       // replace!

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
                return !(lhs == rhs);}

            private:
                // ----
                // data
                // ----
                
                //modified this -> ask if this is okay
                const int* _p;

            public:
                // -----------
                // constructor
                // -----------

                const_iterator (const int* p) {
                    _p = p;}

                // ----------
                // operator *
                //returns the value at p
                // ----------

                const int& operator * () const {
                    //returns a reference to the sentinel 
                    //derefence the pointer/ value at the sentinel
                    return *_p;}

                // -----------
                // operator ++
                // -----------

                const_iterator& operator ++ () {
                    //get abs value at sentinnel (tells you how many bytes are being used)
                    int tmp = abs(*_p);

                    //jumping over beginning sentinnel value
                    //TODO: check if this is the correct way of casting
                    char* tmp_pointer = reinterpret_cast<char*>(_p) + 4;
                    
                    //jumping over the block of bytes in use
                    tmp_pointer += tmp;

                    //add four again to get over the last sentinnel
                    tmp_pointer += 4;

                    //set p to tmp_pointer since we have skipped over block 
                    _p = reinterpret_cast<int*>(tmp_pointer);

                    //return the new spot
                    return *this;}

                // -----------
                // operator ++
                // -----------

                const_iterator operator ++ (int) {
                    const_iterator x = *this;
                    ++*this;
                    return x;}

                // -----------
                // operator --
                // -----------

                const_iterator& operator -- () {
                    //get abs value at sentinnel (tells you how many bytes are being used)
                    //jumping over beginning sentinnel value
                    char* tmp_pointer = reinterpret_cast<char*>(_p) - 4;

                    int* value = reinterpret_cast<int*>(tmp_pointer);

                    int tmp = abs(*value);

                    //TODO: check if this is the correct way of casting

                    //jumping over the block of bytes in use
                    tmp_pointer -= tmp;

                    //add four again to get over the last sentinnel
                    tmp_pointer -= 4;

                    //set p to tmp_pointer since we have skipped over block 
                    _p = reinterpret_cast<int*>(tmp_pointer);

                    //return the new spot
                    return *this;}

                // -----------
                // operator --
                // -----------

                const_iterator operator -- (int) {
                    const_iterator x = *this;
                    --*this;
                    return x;}};

    private:
        // ----
        // data
        // ----

        //N isnt the number of T's, N is the number of bytes in allocator
        //add four extra bytes, TA told us to do this
        char a[N];
        size_type size; 

        // -----
        // valid
        // -----

        /**
         * O(1) in space
         * O(n) in time
         * Need a valid set of blocks
         * March through entire array
         * confirm that the front and rear sentinel match in value and in sign 
         */
        bool valid () const {
            // <your code>
            // <use iterators>
            return true;}

    public:
        // -----------
        // constructor
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
         */
        my_allocator (): a{0} {

            size = N; 

            (*this)[0] = (int)N - 8; // correct
            (*this)[N-4] = (int) N - 8; // correct
            assert(valid());}

        my_allocator             (const my_allocator&) = default;
        ~my_allocator            ()                    = default;
        my_allocator& operator = (const my_allocator&) = default;

        // --------
        // allocate
        // --------

        /**
         * O(1) in space
         * O(n) in time
         * after allocation there must be enough space left for a valid block
         * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
         * choose the first block that fits
         * throw a bad_alloc exception, if n is invalid
         * return the address of the first t the person requeste
         * //char* temp = reinterpret_cast<char*>(&(*b));

        //temp -= 4;

        //int* value = reinterpret_cast<int*>(temp);

        //value = -(num_alloc - 8);

        //set beginning and end of free block


        //b = num_free_after_alloc - 8; 
        //(*this)[index] = num_free_after_alloc - 8;
        //(*this)[index + num_free_after_alloc + sizeof(int)] = num_free_after_alloc - 8;
         */
        pointer allocate (size_type n) {
            //use iterator to iterate through array a 
            iterator b = this -> begin();
            iterator e = this -> end();
            //pointer target = nullptr;

            //cout << "initial free bytes: " << *b << endl;
            //this is correct
            int num_alloc = (n * sizeof(T)) + 8;
            bool found_block = false;
            //iterate through array until you find first free block that's big enough for the
            //allocation request
            //while the block is busy and it's not big enough to use for the allocation request
            //move on to the next block
            int index = 1; 
            while(b != e && !found_block){
                if((*b > 0) && (*b + 8 >= num_alloc)){
                    found_block = true;
                    //cout << "address of target block (&b): " << &b << endl;
                }else{
                    //cout << "before incrementing b: " << &b << endl;
                    b++;
                    //cout << "after incrementing b: " << &b << endl;
                    index++;
                }
            }

            //dereference b and see how many bytes are free
            int num_free = *b - num_alloc;

            //value at b - numbytes to allocate will be the value of the free header
            if(num_free == 0){
                //just switch signs of header and footer sentinnels
                int value = abs(*b);
                int* start = &(*b);
                int* end = start + 1 + (num_alloc - 8 / sizeof(int));
                *start = value; 
                *end = value;
                return reinterpret_cast<T*>(start);
            }else if(num_free < sizeof(T) + (2 * sizeof(int))){
                //if it is less than then just give this amount of bytes to the last allocation?
                num_alloc = num_alloc + sizeof(T) + (2 * sizeof(int));
                //set begining and end sentinal to new val 
                //*b = -(num_alloc);
                int* start = &(*b);
                int* end = start + 1 + (num_alloc - 8 / sizeof(int));
                *start = -(num_alloc);
                *end = -(num_alloc);
                return reinterpret_cast<T*>(start);
            }else{
                //set beginning and end of busy block 
                //allocate -> set the beginning sentinnel, and end sentinnel
                //cout << "num bytes allocated: " << num_alloc - 8 <<endl;
                *b = -(num_alloc);
                int* start = &(*b);
                int* end = start + 1 + (num_alloc - 8 / sizeof(int));
                //*b = -(num_alloc - 8);
                *start = -(num_alloc - 8); 
                *end = -(num_alloc - 8);
                return reinterpret_cast<T*>(start);
                //b++; 
            }
            //cout << "after allocating" << endl;   
            assert(valid());
            return nullptr;}             // replace! -> throw a bad_alloc exception

        // ---------
        // construct
        // ---------

        /**
         * O(1) in space
         * O(1) in time
         */
        void construct (pointer p, const_reference v) {
            new (p) T(v);                               // this is correct and exempt
            assert(valid());}                           // from the prohibition of new

        // ----------
        // deallocate
        // ----------

        /**
         * O(1) in space
         * O(1) in time
         * after deallocation adjacent free blocks must be coalesced
         * throw an invalid_argument exception, if p is invalid
         * takes in a pointer of the index of the busy block
         */

        //TODO: do not use iterators, use pointers
        void deallocate (pointer p, size_type) {
            //if p is invalid
            if(p == nullptr){
                //throw invalid_argument exception
                throw invalid_argument("pointer p is invalid");
            } 

            //boolean flag to check if left and right are free

            //char* tmp = reinterpret_cast<char*>(p);
            // p - 4 in order to find the place of the sentinnel and read in the number
            //of bytes to free
            //tmp -= 4; 
            //case #1: coallesce left side
            //if(left && !right){
                //if adjacent left block is free
                //total_bytes_to_free = *left + 8;
                //we want to also zero out the end sentinnel

                //int num_bytes_to_move = *p 
                //*p = 0; -> setting beginning sentinnel
                //p += 1(bc p is an int pointer) + num_bytes_to_move / 4 + 1;
                //*p = 0 -> setting end sentinnel 
            //}
            //case #2: coallesce right side
            //else if(!left && right){
                //check to see if there is any free right adjacent block to coallesce
                //at the beginning sentinnel of the adjacent block
                //dereference and check if adjacent block is free
            //}
            //case #3 coallesce left and right side
            //else if(left && right)
            //else{
            //case #4: no coallescing
            //p is a pointer to the index of the busy block, p is a pointer to a T
            //we want a pointer to a char because the allocator array is an array 
            //of char's
            cout << "inside deallocate method" << endl;

            cout << "address of p before casting to *int: " << &p <<endl;

            int* headerAddress = reinterpret_cast<int*>(p);

            cout << "made header adress pointer" << endl;

            cout << "address of header address: " << &headerAddress <<endl;

            --headerAddress;

            cout << "address of header address after --: " << &headerAddress <<endl;
            
            cout << "moved header adress to footer" << endl;

            cout << "value at header address: " << *headerAddress <<endl;

            iterator it = iterator(headerAddress);

            *it = abs(*headerAddress);

            //reinterpret the pointer argument to get the char pointer
            //char* tmp = reinterpret_cast<char*>(p);

            // p - 4 in order to find the place of the sentinnel and read in the number
            //of bytes to free
            //tmp -= 4; 

            //change the spot at the beginning sentinnel to be positive (free)
            //need to dereference the pointer to get the num bytes being used at 
            //the moment 
            //use reinterpret cast before dereferencing the new pointer to an int 
            //pointer in order to read the sentinnel value
            //int* value = reinterpret_cast<int*>(tmp);

            //first free the busy block
            //int free_block_bytes = *value / 4;

            //cout << "free_block_bytes in deallocate: " << free_block_bytes << endl;
            //freeing the beginning sentinnel
            //*value = abs(free_block_bytes);

            //moving pointer to end sentinnel
            //value += (1 + (abs(free_block_bytes) / 4)); 

            //freeing end sentinnel
            //*value = abs(free_block_bytes);
            //}
            //check to see if there is any free adjacent block to coallesce
            //int total_bytes_to_free = free_block_bytes + 8;


            //set the beginning sentinnel to total_bytes_to_free
            //at this point we can set the beginning sentinnel to free

            // new attempt

            // first create an iterator at the pointer we were passed in
            // we will use the constructor method to create the pointer in the right space
            //iterator pointer = iterator(p);

            //** WE MIGHT NEED TO DO THE SAME STEPS AS ABOVE IN ORDER TO GET THE POINTER PASSED IN INTO THE HEADER SPOT
            // get the size from the middle sentinal, and since we are freeing it, make sure to turn it into a positive number
            //int middle_size = abs(*p);
            // next we will create a right and left flag to check for coalescing 
            //bool left_free = false;
            //bool right_free = false;
            // next we will create two ints a left size and a right size to keep track of the size of the neighbor blocks
            //int left_size = 0;
            //int right_size = 0;

            // first off we will inc the pointer to the right with a ++ to check if the right block is free, we only need to check the start sent
            // pointer++;
            // if(*pointer>0) {
            //     right_free = true;
            //     right_size = *pointer;
            // }
            // // next we will double decrement the pointer so we can check if the left block is free
            // pointer--;
            // pointer--;
            // if(*pointer >0) {
            //     left_free = true;
            //     left_size = *pointer;
            // }
            // // now we will check the first case: is the left free, because our pointer is currently at the left most block
            // if(left_free && !right_free) {
            //     // the new sentinal will be the value of the left block + 8 (getting rid of 1 set of sents) + the current block's value
            //     // this will create a free block from the left block's starting sent, to the end of the middle block
            //     *pointer = left_size + 8 + middle_size;
            // }
            // // next we will check if both blocks are eligable, because the leftmost block is still needed to do this opperation
            // else if(right_free && left_free) {
            //     // the new sentinal will be the value of the left block + 16 (getting rid of 2 sets of sents) + the current block's value + the right's value
            //     // this will create a free block from the left blocks starting sent, to the end of the middle block
            //     *pointer = left_size + 16 + middle_size + right_size;
            // }
            // // now we will check the third case: is the right free and the left taken?
            // else if(right_free && !left_free) {
            //     // bring the pointer back to the start sent of the middle block
            //     pointer++;
            //     // the new sentinal will be the value of the right block + 8 (getting rid of 1 set of sents) + the current block's value
            //     // this will create a free block from the middle block's starting sent, to the end of the right block
            //     *pointer = right_size + 8 + middle_size;
            // }
            // // base case: no coalescing, just change the current block from taken to free!
            // else {
            //     // bring the pointer back to the start sent of the middle block
            //     pointer++;
            //     // the new sentinal will be the current block's value (freed of course)
            //     // this will create a free block from the middle block's starting sent, to the end of the right block
            //     *pointer = middle_size;
            // }

            // to my understaning, we do not have to overwrite sentinels that are caught in the middle
            // for example, if we coalesce both the left and right blocks, there is no need to destroy/overwrite the old middle sentinels
            // this is because the new sents will tell the pointer to skip over this "free" area

   
            assert(valid());}

            

 
        // -------
        // destroy
        // -------

        /**
         * O(1) in space
         * O(1) in time
         */
        void destroy (pointer p) {
            p->~T();               // this is correct
            assert(valid());}

        // -----------
        // operator []
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         */
        int& operator [] (int i) {
            return *reinterpret_cast<int*>(&a[i]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const int& operator [] (int i) const {
            return *reinterpret_cast<const int*>(&a[i]);}

        // -----
        // begin
        // -----

        /**
         * O(1) in space
         * O(1) in time
         */
        iterator begin () {
            return iterator(&(*this)[0]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator begin () const {
            return const_iterator(&(*this)[0]);}

        // ---
        // end
        // ---

        /**
         * O(1) in space
         * O(1) in time
         */
        iterator end () {
            return iterator(&(*this)[N]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator end () const {
            return const_iterator(&(*this)[N]);}};


#endif // Allocator_h
