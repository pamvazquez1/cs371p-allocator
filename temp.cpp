#include "Allocator.hpp"
#include <iostream>

int main(){

my_allocator<double, 1000> alloc;

alloc.allocate(5);

alloc.allocate(3);

my_allocator<double, 1000>::iterator b = alloc.begin();
my_allocator<double, 1000>::iterator e = alloc.end();

std::cout << "ouside while loop" <<std::endl;

while(b != e){

    std::cout << *b << std::endl;

    b++;
}

}

